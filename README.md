# Azure Infrastructure Setup Guide

This guide outlines the necessary steps to set up your Azure environment using the Azure CLI and Terraform. Follow these instructions carefully. Changes to the names of the resource groups must be reflected in the Terraform configuration files.

## Prerequisites

- Ensure Docker is installed and running on your machine.
- Install the Azure CLI on your machine.

## Steps

### 1. Create Resource Group

Create the resource group in the East US region:

```bash
az group create --name dvopsclhobz --location eastus
```
### 2. Create the Container Registry
Create an Azure Container Registry with a Basic SKU:
```bash
az acr create --resource-group dvopsclhobz --name devopsmiageclhobz --sku Basic
```
### 3. Log in to Azure Container Registry

```bash
az acr login --name devopsmiageclhobz
```

### 4. Enable Admin Rights and Retrieve Credentials
Enable admin rights and retrieve the credentials for the registry:
```bash
az acr update -n devopsmiageclhobz --admin-enabled true
az acr credential show --name devopsmiageclhobz
```
Once the credentials are displayed, copy the username and password. You will need to paste these into the main.tf file located in the myterraformsetup folder in both front end and back end app services:
```bash
"DOCKER_REGISTRY_SERVER_USERNAME" = "YourUsernameHere"
"DOCKER_REGISTRY_SERVER_PASSWORD" = "YourPasswordHere"
```
Ensure to do this for both the backend and frontend app services.

### 5. Build and Push Docker Images
Build the Frontend Docker Image
Navigate to your frontend directory and build the Docker image:
```bash
cd path/to/your/frontend
docker build -t devopsmiageclhobz.azurecr.io/myapp-frontend:v1 .
```
Build the Backend Docker Image
Similarly, navigate to your backend directory and build the Docker image:
```bash
cd path/to/your/backend
docker build -t devopsmiageclhobz.azurecr.io/myapp-backend:v1 .
```
Push the Images to ACR
```bash
docker push devopsmiageclhobz.azurecr.io/myapp-frontend:v1
docker push devopsmiageclhobz.azurecr.io/myapp-backend:v1
```


### 6. Set Up Firewall Rule for Local Access
Visit What Is My IP Address to find your public IPv4 address. Add this IP address to the allow_client_ip firewall rule in your main.tf file to enable local access to the PostgreSQL database on Azure for launching the init.sql script. You should add the ipv4 address both in the start_ip_address and end_ip_address fields.

### 7. Initialize Terraform
Navigate to the myterraformsetup folder:
```bash
cd myterraformsetup
```
Initialize Terraform and apply the configuration:
```bash
terraform init
terraform apply
```

