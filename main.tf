provider "azurerm" {
  features {}
}

resource "azurerm_postgresql_server" "example" {
  name            	= "clhobzdb"
  location        	= "eastus"
  resource_group_name = "dvopsclhobz" # Reference the existing resource group

  sku_name        	= "GP_Gen5_2"
  version         	= "11"
  administrator_login = "postgres"
  administrator_login_password = "Herveflop33!"

  storage_mb            	= 5120
  backup_retention_days 	= 7
  geo_redundant_backup_enabled = false
  public_network_access_enabled = true
  infrastructure_encryption_enabled = false

  ssl_enforcement_enabled = false
  ssl_minimal_tls_version_enforced = "TLSEnforcementDisabled"
}

resource "azurerm_postgresql_firewall_rule" "example" {
  name            	= "allow_client_ip"
  resource_group_name = azurerm_postgresql_server.example.resource_group_name
  server_name     	= azurerm_postgresql_server.example.name
  start_ip_address	= "86.206.34.74"
  end_ip_address  	= "86.206.34.74"
}

resource "azurerm_postgresql_firewall_rule" "allow_azure_ips" {
  name                = "AllowAzureIps"
  resource_group_name = azurerm_postgresql_server.example.resource_group_name
  server_name         = azurerm_postgresql_server.example.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}

resource "null_resource" "init_script" {
  depends_on = [azurerm_postgresql_firewall_rule.allow_azure_ips, azurerm_postgresql_firewall_rule.example, azurerm_postgresql_server.example]
  provisioner "local-exec" {
	command = <<-EOT
  	$env:PGPASSWORD='Herveflop33!'
  	psql -h clhobzdb.postgres.database.azure.com -U postgres@clhobzdb -d postgres -f init.sql
	EOT

	interpreter = ["PowerShell", "-Command"]
  }
}

# Data source for Azure Container Registry
data "azurerm_container_registry" "example" {
  name                = "devopsmiageclhobz"
  resource_group_name = "dvopsclhobz"
}

# Azure App Service Plan
resource "azurerm_service_plan" "example" {
  name                = "clhobz-app-service-plan"
  location            = "East US"
  resource_group_name = "dvopsclhobz"
  os_type                = "Linux"
  sku_name            = "B1" 
}

# Azure App Service
resource "azurerm_app_service" "example" {
  name                = "clhobz-web-app-backend"
  location            = azurerm_service_plan.example.location
  resource_group_name = azurerm_service_plan.example.resource_group_name
  app_service_plan_id = azurerm_service_plan.example.id

  # Define dependencies to ensure order of creation
  depends_on = [
    azurerm_postgresql_server.example,
    azurerm_postgresql_firewall_rule.allow_azure_ips,
    azurerm_postgresql_firewall_rule.example,
    null_resource.init_script
  ]

  app_settings = {
    "DOCKER_REGISTRY_SERVER_URL"      = data.azurerm_container_registry.example.login_server
    "DOCKER_REGISTRY_SERVER_USERNAME" = "devopsmiageclhobz"
    "DOCKER_REGISTRY_SERVER_PASSWORD" = "lAuv+rBrCaMFEg+hL10dzrpLcnhK1KTeCf3bD8n2Vw+ACRBWNSBE"
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE" = "false"
    "DB_HOST" = "clhobzdb.postgres.database.azure.com"
    "DB_PORT" = "5432"
    "DB_USER" = "postgres@clhobzdb"
    "DB_PASSWORD" = "Herveflop33!"
    "DB_DATABASE" = "darce_side_bd"
    "DB_SSL" = "true"
  }

  site_config {
    linux_fx_version = "DOCKER|${data.azurerm_container_registry.example.login_server}/myapp-backend:v1"
  }
}

# Azure App Service for the Frontend
resource "azurerm_app_service" "frontend" {
  name                = "clhobz-web-app-frontend"
  location            = azurerm_service_plan.example.location
  resource_group_name = azurerm_service_plan.example.resource_group_name
  app_service_plan_id = azurerm_service_plan.example.id

  # Ensure it is created after the backend and other dependencies
  depends_on = [
    azurerm_app_service.example
  ]

  app_settings = {
    "DOCKER_REGISTRY_SERVER_URL"      = data.azurerm_container_registry.example.login_server
    "DOCKER_REGISTRY_SERVER_USERNAME" = "devopsmiageclhobz"
    "DOCKER_REGISTRY_SERVER_PASSWORD" = "lAuv+rBrCaMFEg+hL10dzrpLcnhK1KTeCf3bD8n2Vw+ACRBWNSBE"
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE" = "false"
  }

  site_config {
    linux_fx_version = "DOCKER|${data.azurerm_container_registry.example.login_server}/myapp-frontend:v1"
  }
}

# Output the default site hostname of the Frontend App Service
output "app_service_frontend_default_site_hostname" {
  value = azurerm_app_service.frontend.default_site_hostname
}

# Output the fully qualified domain name (FQDN) of the PostgreSQL Server
output "postgresql_server_fqdn" {
  value = azurerm_postgresql_server.example.fqdn
}

# Output the default site hostname of the App Service
output "app_service_default_site_hostname" {
  value = azurerm_app_service.example.default_site_hostname
}
